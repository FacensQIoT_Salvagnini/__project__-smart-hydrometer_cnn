import cv2
import os

content_data = os.listdir('data_black')

for path_data in content_data:
    path_fold = "data_black/" + path_data
    path_class = os.listdir(path_fold)
    
    for c in path_class:
        path_to_the_class = path_fold + '/' + c
        class_content = os.listdir(path_to_the_class)
        # print(path_to_the_class)
        
        for img in class_content:
            image_path = path_to_the_class + '/' + img
            
            if "jpg" in image_path:
                img = cv2.imread(image_path, 1)
                img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                cv2.imwrite(image_path, img)
        

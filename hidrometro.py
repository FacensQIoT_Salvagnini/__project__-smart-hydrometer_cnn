import cv2
import imutils
from datetime import datetime

vobj = cv2.VideoCapture("http://172.16.100.106/api/mjpegvideo.cgi?")

# ROIS 30 x 56
D1 = ((255, 190), (285, 246))
D2 = ((311, 190), (341, 246))
D3 = ((365, 190), (395, 246))
D4 = ((420, 190), (450, 246))
D5 = ((473, 190), (503, 246))
D6 = ((526, 190), (556, 246))

# print(D1[0][0] - D1[1][0], D1[0][1] - D1[1][1])
# print(D2[0][0] - D2[1][0], D2[0][1] - D2[1][1])
# print(D3[0][0] - D3[1][0], D3[0][1] - D3[1][1])
# print(D4[0][0] - D4[1][0], D4[0][1] - D4[1][1])
# print(D5[0][0] - D5[1][0], D5[0][1] - D5[1][1])
# print(D6[0][0] - D6[1][0], D6[0][1] - D6[1][1])

def img_filter(img, filter_id):
    if filter_id == 0:
        return img
    elif filter_id == 1:    
        return cv2.blur(img,(5, 5)) # Média
    elif filter_id == 2:
        return cv2.GaussianBlur(img,(5, 5), 3) # Blur Gaussiano
    elif filter_id == 3:
        return cv2.medianBlur(img, 7) # Blur pela média  (cv2.medianBlur(img, 7)) => Bom resultado
    elif filter_id == 4:
        return cv2.bilateralFilter(img, 9, 20, 90) # Filtro bilateral cv2.bilateralFilter(img, 9, 30, 90) => Bom resultado
    elif filter_id == 5:
        return cv2.fastNlMeansDenoisingColored(img, 7, 3, 3, 9, 21) # Boa saída mas utiliza muito recurso computacional
i = 0
start_time = datetime.now()
status = False

while True:
    time_now = datetime.now()
    rois_list = []
    
    while status is not True:
        status, frame = vobj.read()
    status = False

    frame = imutils.rotate(frame, angle = -96.6544)
    old_frame = frame.copy()

    frame = img_filter(frame, 4)

    # ROIS das regiões de interesse 
    rois_list.append(frame[D1[0][1]:D1[1][1], D1[0][0]:D1[1][0]])
    rois_list.append(frame[D2[0][1]:D2[1][1], D2[0][0]:D2[1][0]])
    rois_list.append(frame[D3[0][1]:D3[1][1], D3[0][0]:D3[1][0]])
    rois_list.append(frame[D4[0][1]:D4[1][1], D4[0][0]:D4[1][0]])
    rois_list.append(frame[D5[0][1]:D5[1][1], D5[0][0]:D5[1][0]])
    rois_list.append(frame[D6[0][1]:D6[1][1], D6[0][0]:D6[1][0]])
    
    if (time_now - start_time).seconds > 10:
        for j, img in enumerate(rois_list):
            cv2.imwrite('dataset/d{}/{}.jpg'.format(j + 1, i), img)
        start_time = datetime.now()
        i += 1

    cv2.imshow("Output", frame)
    cv2.imshow("Output_without_filter", old_frame)
    cv2.imshow("D1", rois_list[0])
    cv2.imshow("D2", rois_list[1])
    cv2.imshow("D3", rois_list[2])
    cv2.imshow("D4", rois_list[3])
    cv2.imshow("D5", rois_list[4])
    cv2.imshow("D6", rois_list[5])

    key = cv2.waitKey(1) & 0xFF

    if key == ord("q"):
        break

vobj.release()
cv2.destroyAllWindows()
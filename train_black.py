import numpy as np
import matplotlib.pyplot as plt
import glob
import shutil
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Conv2D, Flatten, Dropout, MaxPooling2D
from tensorflow.keras.preprocessing.image import ImageDataGenerator

# Pretify das barras de progresso do treinamento
import tqdm
import tqdm.auto
tqdm.tqdm = tqdm.auto.tqdm


# Especifica os diretórios das imagens e as prepara para treinamento
train_dir = '/home/fsalvagnini/Documents/Qualcomm/Hidrometro/data_black/treino'
val_dir = '/home/fsalvagnini/Documents/Qualcomm/Hidrometro/data_black/teste'

batch_size = 50

# Treinamento 
image_gen_train = ImageDataGenerator(rescale = 1./255)
train_data_gen = image_gen_train.flow_from_directory(batch_size = batch_size,
                                                    directory = train_dir,
                                                    color_mode='grayscale',
                                                    shuffle = True,
                                                    target_size = (56, 30), # height x width
                                                    class_mode = 'sparse')

# Teste
image_gen_val = ImageDataGenerator(rescale = 1./255)
val_data_gen = image_gen_val.flow_from_directory(batch_size=batch_size, 
                                                 directory=val_dir, 
                                                 color_mode='grayscale',
                                                 target_size=(56, 30), # height x width
                                                 class_mode='sparse')

# Instancia a arquitetura da rede
model = Sequential()

model.add(Conv2D(4, 3, padding='same', activation='relu', input_shape=(56, 30, 1))) # batch, height, width, channels
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(8, 3, padding='same', activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Flatten())
#model.add(Dropout(0.2))
model.add(Dense(256, activation='relu'))

#model.add(Dropout(0.2))
model.add(Dense(10, activation='softmax'))

model.compile(optimizer='adam', 
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

# Treina
epochs = 25
history = model.fit_generator(
    train_data_gen,
    steps_per_epoch=int(np.ceil(train_data_gen.n / float(batch_size))),
    epochs=epochs,
    validation_data=val_data_gen,
    validation_steps=int(np.ceil(val_data_gen.n / float(batch_size)))
)

model.save('hydrometer_model_black.h5')

# Exibe o desempenho do modelo

acc = history.history['acc']
val_acc = history.history['val_acc']

loss = history.history['loss']
val_loss = history.history['val_loss']

epochs_range = range(epochs)

plt.figure(figsize=(8, 8))
plt.subplot(1, 2, 1)
plt.plot(epochs_range, acc, label='Training Accuracy')
plt.plot(epochs_range, val_acc, label='Validation Accuracy')
plt.legend(loc='lower right')
plt.title('Training and Validation Accuracy')

plt.subplot(1, 2, 2)
plt.plot(epochs_range, loss, label='Training Loss')
plt.plot(epochs_range, val_loss, label='Validation Loss')
plt.legend(loc='upper right')
plt.title('Training and Validation Loss')
plt.show()